sh createCa.sh

sleep 5

echo "########## Setting required env ############"
export CHANNEL_NAME=autochannel
export IMAGE_TAG=latest
export COMPOSE_PROJECT_NAME=fabricscratch-ca

echo "########## Generate the genesis block ############"
configtxgen -profile OrdererGenesis \
-channelID system-channel -outputBlock \
./channel-artifacts/genesis.block

sleep 2

echo "########## Generate the Channel Transaction ############"
configtxgen -profile AutoChannel \
-outputCreateChannelTx ./channel-artifacts/$CHANNEL_NAME.tx \
-channelID $CHANNEL_NAME

sleep 2

echo "########## Starting the components ############"
docker-compose -f docker/docker-compose-singlepeer.yml up -d

export ORDERER_TLS_CA=`docker exec cli  env | grep ORDERER_TLS_CA | cut -d'=' -f2`

sleep 2

echo "########## Creating the Channel ############"
docker exec cli peer channel create -o orderer.auto.com:7050 \
-c $CHANNEL_NAME -f /opt/gopath/src/github.com/hyperledger/fabric/peer/config/$CHANNEL_NAME.tx \
--tls --cafile $ORDERER_TLS_CA


sleep 2

echo "########## Joining Charity Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=CharityMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.charity.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/users/Admin@charity.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2

echo "########## Joining Donator Peer to Channel ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=DonatorMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.donator.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/users/Admin@donator.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel join -b $CHANNEL_NAME.block

sleep 2


echo "########## Generating anchor peer tx for charity ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/CharityMSPanchors.tx -channelID autochannel -asOrg CharityMSP

sleep 2

echo "########## Generating anchor peer tx for donator ############"
configtxgen -profile AutoChannel -outputAnchorPeersUpdate ./channel-artifacts/DonatorMSPanchors.tx -channelID autochannel -asOrg DonatorMSP

sleep 2

echo "########## Anchor Peer Update for Charity ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=CharityMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.charity.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/users/Admin@charity.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.auto.com:7050 -c autochannel -f ./config/CharityMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2

echo "##########  Anchor Peer Update for Donator ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=DonatorMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.donator.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/users/Admin@donator.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer channel update -o orderer.auto.com:7050 -c autochannel -f ./config/DonatorMSPanchors.tx --tls --cafile $ORDERER_TLS_CA

sleep 2


echo "##########  Package Chaincode ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=CharityMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.charity.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/users/Admin@charity.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode package charity.tar.gz --path /opt/gopath/src/github.com/chaincode/Charity/ --lang node --label charity_1

sleep 2

echo "##########  Install Chaincode on Charity peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=CharityMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.charity.auto.com:7051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/charity.auto.com/users/Admin@charity.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install charity.tar.gz


sleep 2

echo "##########  Install Chaincode on Donator peer ############"
docker exec \
     -e CORE_PEER_LOCALMSPID=DonatorMSP \
     -e CHANNEL_NAME=autochannel \
     -e CORE_PEER_ADDRESS=peer0.donator.auto.com:9051 \
     -e ORDERER_TLS_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem \
     -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.crt \
     -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.key \
     -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/ca.crt \
     -e CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/auto.com/donator.auto.com/users/Admin@donator.auto.com/msp \
     -e CORE_PEER_TLS_ENABLED=true \
     -e FABRIC_LOGGING_SPEC=INFO \
     -i \
     cli peer lifecycle chaincode install charity.tar.gz

sleep 2
