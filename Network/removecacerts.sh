#!/bin/bash


sudo rm -rf \
    ./organizations/fabric-ca/donator/msp \
    ./organizations/fabric-ca/donator/ca-cert.pem \
    ./organizations/fabric-ca/donator/fabric-ca-server.db \
    ./organizations/fabric-ca/donator/IssuerPublicKey \
    ./organizations/fabric-ca/donator/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/donator/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/charity/msp \
    ./organizations/fabric-ca/charity/ca-cert.pem \
    ./organizations/fabric-ca/charity/fabric-ca-server.db \
    ./organizations/fabric-ca/charity/IssuerPublicKey \
    ./organizations/fabric-ca/charity/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/charity/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/orderer/msp \
    ./organizations/fabric-ca/orderer/ca-cert.pem \
    ./organizations/fabric-ca/orderer/fabric-ca-server.db \
    ./organizations/fabric-ca/orderer/IssuerPublicKey \
    ./organizations/fabric-ca/orderer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/orderer/tls-cert.pem

sudo rm -rf \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/msp \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/fabric-ca-client-config.yaml \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers \
    ./organizations/ordererOrganizations/auto.com/orderer.auto.com/users
    
sudo rm -rf \
    ./organizations/peerOrganizations/auto.com/charity.auto.com/ca \
    ./organizations/peerOrganizations/auto.com/charity.auto.com/msp \
    ./organizations/peerOrganizations/auto.com/charity.auto.com/peers \
    ./organizations/peerOrganizations/auto.com/charity.auto.com/tlsca \
    ./organizations/peerOrganizations/auto.com/charity.auto.com/users \
    ./organizations/peerOrganizations/auto.com/charity.auto.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/auto.com/donator.auto.com/ca \
    ./organizations/peerOrganizations/auto.com/donator.auto.com/msp \
    ./organizations/peerOrganizations/auto.com/donator.auto.com/peers \
    ./organizations/peerOrganizations/auto.com/donator.auto.com/tlsca \
    ./organizations/peerOrganizations/auto.com/donator.auto.com/users \
    ./organizations/peerOrganizations/auto.com/donator.auto.com/fabric-ca-client-config.yaml
