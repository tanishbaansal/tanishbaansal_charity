echo "##########Strating the docker-compose-ca############"
export COMPOSE_PROJECT_NAME=fabricscratch-ca
docker-compose -f ./docker/docker-compose-ca.yml up -d 

echo "##########Creating the folder structure for CA############"

mkdir -p organizations/peerOrganizations/auto.com/charity.auto.com/
mkdir -p organizations/peerOrganizations/auto.com/donator.auto.com/

echo "##########Setting PATH for Fabric CA client############"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/
sleep 2

echo "########## Enroll Charity ca admin############"

fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-charity --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem

sleep 5

echo "########## Create config.yaml for Charity ############"

echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-charity.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-charity.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-charity.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-charity.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/msp/config.yaml

echo "########## Register charity peer ############"
fabric-ca-client register --caname ca-charity --id.name peer0charity --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem
sleep 5

echo "########## Register charity user ############"

fabric-ca-client register --caname ca-charity --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem
sleep 5

echo "########## Register charity Org admin ############"

fabric-ca-client register --caname ca-charity --id.name charityadmin --id.secret charityadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem
sleep 5

echo "########## Generate Charity peer MSP ############"

fabric-ca-client enroll -u https://peer0charity:peer0pw@localhost:7054 --caname ca-charity -M ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/msp --csr.hosts peer0.charity.auto.com --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem
sleep 5

echo "########## Generate Charity Peer tls cert ############"

fabric-ca-client enroll -u https://peer0charity:peer0pw@localhost:7054 --caname ca-charity -M ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls --enrollment.profile tls --csr.hosts peer0.charity.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem
sleep 5

echo "########## Organizing the folders ############"
echo "########## Copy the certificate files ############"

cp organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/tlscacerts/* organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/ca.crt
cp organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/signcerts/* organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.crt
cp organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/keystore/* organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/tlsca
cp ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/tlsca/tls-localhost-7054-ca-charity.pem

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/ca
cp ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/ca/localhost-7054-ca-charity.pem

cp organizations/peerOrganizations/auto.com/charity.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/charity.auto.com/peers/peer0.charity.auto.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-charity -M ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/users/User1@charity.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem

sleep 5

cp organizations/peerOrganizations/auto.com/charity.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/charity.auto.com/users/User1@charity.auto.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://charityadmin:charityadminpw@localhost:7054 --caname ca-charity -M ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/users/Admin@charity.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/charity/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/auto.com/charity.auto.com/users/Admin@charity.auto.com/msp/config.yaml

echo "============================================================End of Charity =================================================================================================="

echo "================================================================== Donator =================================================================================================="


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/

echo "enroll Donator ca"
echo "======================"

fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-donator --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem

sleep 5




echo "Create config.yaml for Donator" 
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-donator.pem 
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-donator.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-donator.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-donator.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/msp/config.yaml

echo "Register donator peer"
echo "=========================="
fabric-ca-client register --caname ca-donator --id.name peer0donator --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem
sleep 5

echo "Register donator user"
echo "=========================="
fabric-ca-client register --caname ca-donator --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem
sleep 5

echo "Register donator Org admin"
echo "=========================="
fabric-ca-client register --caname ca-donator --id.name donatoradmin --id.secret donatoradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem
sleep 5

echo "Generate Donator peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0donator:peer0pw@localhost:8054 --caname ca-donator -M ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/msp --csr.hosts peer0.donator.auto.com --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem
sleep 5

echo "Generate Donator Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0donator:peer0pw@localhost:8054 --caname ca-donator -M ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls --enrollment.profile tls --csr.hosts peer0.donator.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/tlscacerts/* organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/ca.crt
cp organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/signcerts/* organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.crt
cp organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/keystore/* organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/tlsca
cp ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/tlsca/tls-localhost-8054-ca-donator.pem

mkdir -p ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/ca
cp ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/ca/localhost-8054-ca-donator.pem

cp organizations/peerOrganizations/auto.com/donator.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/donator.auto.com/peers/peer0.donator.auto.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-donator -M ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/users/User1@donator.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/auto.com/donator.auto.com/msp/config.yaml organizations/peerOrganizations/auto.com/donator.auto.com/users/User1@donator.auto.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://donatoradmin:donatoradminpw@localhost:8054 --caname ca-donator -M ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/users/Admin@donator.auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/donator/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/auto.com/donator.auto.com/users/Admin@donator.auto.com/msp/config.yaml


echo "==========================================================End of Donator ========================================================================================================"


echo "==========================================================Orderer Config ========================================================================================================================"


echo "enroll Orderer ca"
echo "================="

mkdir -p organizations/ordererOrganizations/auto.com/orderer.auto.com/


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9052 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5
echo "Create config.yaml for Mvd "
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml

echo "Register orderer"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name orderer0 --id.secret orderer0pw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name ordereradmin --id.secret ordereradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Generate Orderer MSP"
echo "=============================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp --csr.hosts orderer.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/config.yaml

echo "Generate Orderer TLS certificates"
echo "==================================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls --enrollment.profile tls --csr.hosts orderer.auto.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"
echo "Copy the certificate files"
echo "=========================="

  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/server.key

  mkdir -p ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem

  mkdir -p ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/tlscacerts/tlsca.auto.com-cert.pem

  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/orderers/orderer.auto.com/msp/config.yaml

echo "Generate Orderer admin MSP"
echo "=========================="
  fabric-ca-client enroll -u https://ordereradmin:ordereradminpw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/users/Admin@auto.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

  cp ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/auto.com/orderer.auto.com/users/Admin@auto.com/msp/config.yaml

echo "=========================================================End of Mvd ========================================================================================================================"







