#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${ORGMSP}/$6/" \
        ccp-template.json
}

ORG=charity
P0PORT=7051
CAPORT=7054
PEERPEM=../organizations/peerOrganizations/auto.com/charity.auto.com/tlsca/tls-localhost-7054-ca-charity.pem
CAPEM=../organizations/peerOrganizations/auto.com/charity.auto.com/ca/localhost-7054-ca-charity.pem
ORGMSP=Manufacturer

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-charity.json

ORG=donator
P0PORT=9051
CAPORT=8054
PEERPEM=../organizations/peerOrganizations/auto.com/donator.auto.com/tlsca/tls-localhost-8054-ca-donator.pem
CAPEM=../organizations/peerOrganizations/auto.com/donator.auto.com/ca/localhost-8054-ca-donator.pem
ORGMSP=Donator

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-donator.json
