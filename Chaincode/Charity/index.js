/*
 * SPDX-License-Identifier: Apache-2.0
 */

"use strict";

const CharityContract = require("./lib/charity-contract");
const DonationRecordsContract = require("./lib/donation-records-contract");

module.exports.CharityContract = CharityContract;
module.exports.DonationRecordsContract = DonationRecordsContract;
module.exports.contracts = [CharityContract, DonationRecordsContract];
