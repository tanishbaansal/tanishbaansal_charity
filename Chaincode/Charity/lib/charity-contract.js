/*
 * SPDX-License-Identifier: Apache-2.0
 */

"use strict";

const { Contract } = require("fabric-contract-api");

class CharityContract extends Contract {
    async charityExists(ctx, charityId) {
        const buffer = await ctx.stub.getState(charityId);
        return !!buffer && buffer.length > 0;
    }

    async createCharity(ctx, charityId, name, age, amount) {
        const exists = await this.charityExists(ctx, charityId);
        if (exists) {
            throw new Error(`The charity ${charityId} already exists`);
        }
        const asset = {
            charityId: charityId,
            name: name,
            age: age,
            amount: amount,
        };
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(charityId, buffer);
    }

    async readCharity(ctx, charityId) {
        const exists = await this.charityExists(ctx, charityId);
        if (!exists) {
            throw new Error(`The charity ${charityId} does not exist`);
        }
        const buffer = await ctx.stub.getState(charityId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async getCharityByRange(ctx, startKey, endKey) {
        const iterator = await ctx.stub.getStateByRange(startKey, endKey);
        const result = await this.getAllResult(iterator);
        return JSON.stringify(result);
    }
    async getCharityHistory(ctx, charityId) {
        const iterator = await ctx.stub.getHistoryForKey(charityId);
        const result = await this.getAllResult(iterator);
        return JSON.stringify(result);
    }

    async getAllResult(iterator, isHistory) {
        let allResult = [];

        for (
            let res = await iterator.next();
            !res.done;
            res = await iterator.next()
        ) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};

                if (isHistory && isHistory === true) {
                    jsonRes.output = res.value;
                } else {
                    jsonRes.key = res.value.key;
                    jsonRes.Charity = res.value.value.toString("utf8");
                }
                allResult.push(jsonRes);
            }
        }
        return allResult;
    }

    async queryAllOrders(ctx) {
        const queryString = JSON.stringify({
            selector: {},
        });

        const iterator = await ctx.stub.getQueryResult(queryString);
        const result = await this.getAllResult(iterator, false);
        return JSON.stringify(result);
    }

    async getCharityWithPagination(ctx, pagesize, bookmark) {
        const queryString = {
            selector: {},
        };

        const pages = parseInt(pagesize, 10);

        const { iterator, metadata } =
            await ctx.stub.getQueryResultWithPagination(
                JSON.stringify(queryString),
                pages,
                bookmark
            );

        const result = await this.getAllResult(iterator, false);

        let results = {};
        results.result = result;
        results.ResponseMetaData = {
            CharityCount: metadata.fetched_Charitys_count,
            Bookmark: metadata.bookmark,
        };

        return JSON.stringify(results);
    }
}

module.exports = CharityContract;
