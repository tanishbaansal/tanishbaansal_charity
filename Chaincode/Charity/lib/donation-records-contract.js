/*
 * SPDX-License-Identifier: Apache-2.0
 */

"use strict";

const { Contract } = require("fabric-contract-api");
//const crypto = require('crypto');

async function getCollectionName(ctx) {
    return "CollectionOrder";
}

class donationRecordsContract extends Contract {
    async charityRecordsExists(ctx, charityRecordsId) {
        const collectionName = "CollectionOrder";
        const data = await ctx.stub.getPrivateDataHash(
            collectionName,
            charityRecordsId
        );
        return !!data && data.length > 0;
    }
    /*
params: patientID, hospitalID, medicalReport, prescription, cost
*/
    async createcharityRecords(ctx, charityRecordsId) {
        const exists = await this.charityRecordsExists(ctx, charityRecordsId);
        if (exists) {
            throw new Error(
                `The asset charity records ${charityRecordsId} already exists`
            );
        }

        //const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (
            transientData.size === 0 ||
            !transientData.has("charityId") ||
            !transientData.has("donatorId") ||
            !transientData.has("charityReport") ||
            !transientData.has("amount")
        ) {
            throw new Error(
                "The privateValue key was not specified in transient data. Please try again."
            );
        }

        const recordAsset = {};

        recordAsset.charityId = transientData.get("charityId").toString();
        recordAsset.donatorId = transientData.get("donatorId").toString();
        recordAsset.charityReport = transientData
            .get("charityReport")
            .toString();
        recordAsset.amount = transientData.get("amount");
        await ctx.stub.putPrivateData(
            "CollectionOrder",
            charityRecordsId,
            Buffer.from(JSON.stringify(recordAsset))
        );
    }

    async readcharityRecords(ctx, charityRecordsId) {
        const exists = await this.charityRecordsExists(ctx, charityRecordsId);
        if (!exists) {
            throw new Error(
                `The asset charity records ${charityRecordsId} does not exist`
            );
        }
        //let privateDataString;
        // const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData(
            "CollectionOrder",
            charityRecordsId
        );

        return JSON.parse(privateData.toString());
    }

    async deletecharityRecords(ctx, charityRecordsId) {
        const exists = await this.charityRecordsExists(ctx, charityRecordsId);
        if (!exists) {
            throw new Error(
                `The asset charity records ${charityRecordsId} does not exist`
            );
        }
        const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData("CollectionOrder", charityRecordsId);
    }
}

module.exports = donationRecordsContract;
