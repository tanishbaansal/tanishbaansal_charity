/*
 * SPDX-License-Identifier: Apache-2.0
 */

"use strict";

const { ChaincodeStub, ClientIdentity } = require("fabric-shim");
const { CharityContract } = require("..");
const winston = require("winston");

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {
    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon
                .stub()
                .returns(
                    sinon.createStubInstance(winston.createLogger().constructor)
                ),
            setLevel: sinon.stub(),
        };
    }
}

describe("CharityContract", () => {
    let contract;
    let ctx;

    beforeEach(() => {
        contract = new CharityContract();
        ctx = new TestContext();
        ctx.stub.getState
            .withArgs(100)
            .resolves(
                Buffer.from(
                    '{"charityId":100,"name":"Tanish","age":22,"amount":100000}'
                )
            );
    });

    describe("#charityExists", () => {
        it("should return true for a charity", async () => {
            await contract.charityExists(ctx, 100).should.eventually.be.true;
        });

        it("should return false for a charity that does not exist", async () => {
            await contract.charityExists(ctx, 1003).should.eventually.be.false;
        });
    });

    describe("#createCharity", () => {
        it("should create a charity", async () => {
            await contract.createCharity(ctx, 1003, "Tanish", 22, 100000);
            ctx.stub.putState.should.have.been.calledOnceWithExactly(
                1003,
                Buffer.from(
                    '{"charityId":1003,"name":"Tanish","age":22,"amount":100000}'
                )
            );
        });

        it("should throw an error for a charity that already exists", async () => {
            await contract
                .createCharity(ctx, 100, "Tanish", 22, 100000)
                .should.be.rejectedWith("The charity 100 already exists");
        });
    });

    describe("#readCharity", () => {
        it("should return a charity", async () => {
            await contract
                .readCharity(ctx, 100)
                .should.eventually.deep.equal({
                    charityId: 100,
                    name: "Tanish",
                    age: 22,
                    amount: 100000,
                });
        });

        it("should throw an error for a charity that does not exist", async () => {
            await contract
                .readCharity(ctx, 1003)
                .should.be.rejectedWith("The charity 1003 does not exist");
        });
    });

    // describe('#updateCharity', () => {

    //     it('should update a charity', async () => {
    //         await contract.updateCharity(ctx, '1001', 'charity 1001 new value');
    //         ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"charity 1001 new value"}'));
    //     });

    //     it('should throw an error for a charity that does not exist', async () => {
    //         await contract.updateCharity(ctx, '1003', 'charity 1003 new value').should.be.rejectedWith(/The charity 1003 does not exist/);
    //     });

    // });

    // describe('#deleteCharity', () => {

    //     it('should delete a charity', async () => {
    //         await contract.deleteCharity(ctx, '1001');
    //         ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
    //     });

    //     it('should throw an error for a charity that does not exist', async () => {
    //         await contract.deleteCharity(ctx, '1003').should.be.rejectedWith(/The charity 1003 does not exist/);
    //     });

    // });
});
